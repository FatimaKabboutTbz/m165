![](/KN01/images/)

# A

### MongoDBCompass

![mongo_dbs](/KN01/images/mongo_dbs.png)

### Connection String

`mongodb://admin:admin@44.223.175.244:27017/?_authSource=admin_&readPreference=primary&ssl=false&directConnection=true`

Die Option authSource=admin in einem Connection String gibt an, welchen DB-User für die Authentifizierung verwendet werden soll. Dies is so korrekt, weil wir in unserem Fall eine Benutzre "admin" in der Datenbank haben.

### sed Befehle

1.  `sudo sed -i 's/#security:/security:\n authorization: enabled/g' /etc/mongod.conf`

    - Dieser Befehl sucht in der Konfigurationsdatei von MongoDB (/etc/mongod.conf) nach der Zeichenfolge #security: und ersetzt sie durch security:\n authorization: enabled. Dies aktiviert die Autorisierung in der MongoDB-Instanz, was bedeutet, dass Benutzer sich anmelden müssen, um auf die Datenbank zuzugreifen.

2.  `sudo sed -i 's/127.0.0.1/0.0.0.0/g' /etc/mongod.conf`

    - Dieser Befehl ändert in der MongoDB-Konfigurationsdatei (/etc/mongod.conf) die Bind-IP-Adresse von 127.0.0.1 auf 0.0.0.0. Das bedeutet, dass MongoDB auf allen verfügbaren IP-Adressen des Servers lauscht, nicht nur auf der lokalen IP-Adresse. Dies ermöglicht es anderen Geräten im Netzwerk, auf die MongoDB-Instanz zuzugreifen.

![changed_values](/KN01/images/changed_values.png)

# B

### Screenshot (oder JSON) Ihres einzufügenden Dokuments (bevor Sie es einfügen)

![JSON before](./kn01-b.json)

![](/KN01/images/json_before.png)

### Screenshot Ihrer Compass-Applikation mit der Datenbank, Collection und Dokument sichtbar.

![](/KN01/images/db_doc.png)

### Erklärung zum Datentyp vom exportierten File

![exported JSON](./kn01-b-exported.json)
Wenn das Datum nicht als Date im UI gespeichert wurde, wurde es wahrscheinlich als String gespeichert. Wenn Sie das Datum explizit als Objekt mit dem Schlüssel $date angeben und die Datumszeichenfolge im ISO 8601-Format bereitstellen, wird es von MongoDB als Datumsobjekt erkannt, wenn es in der Sammlung gespeichert wird.

![mongodbcompass_date](/KN01/images/mongodbcompass_date.png)

# C

### Compass Commands

![compass_commands](/KN01/images/compass_commands.png)

### Linux Commands

![linux_commands](/KN01/images/linux_commands.png)

### Commands Erklärung

- `show dbs;` `show databases;`
  - Diese Befehle zeigen alle Datenbanken an.
- `use Fatima;`
  - Dieser Befehl wechselt zur angegebenen Collection.
- `show collections;`
  - Dieser Befehl zeigt alle Collections resp. alle Tabellen der Datenbank an.
- `show tables;`
  - Dieser Befehl zeigt alle Tabellen der Datenbank an.
- Unterschied zwischen `show collections;` und `show tables;`
  - Es gibt keinen Unterschied in MongoDB sind Tables Collections.
