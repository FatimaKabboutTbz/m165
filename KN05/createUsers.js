use kn02;
db.createUser({
    user: 'Bob',
    pwd: '1234',
    roles: [
        {role: "read", db: "kn02"}
    ]
})

use admin;
db.createUser({
    user: 'Harry',
    pwd: '1234',
    roles: [
        {role: "readWrite", db: "kn02"}
    ]
})
