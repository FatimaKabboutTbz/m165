# A
![connection error](./Images/connection_error.png)
- [Skript zum erstellen von User](./createUsers.js)

### User 1
#### Login
![user1_login](./Images/user1_login.png)
#### Read
![user1_read](./Images/user1_read.png)
#### Write
![user1_write_error](./Images/user1_write_error.png)
### User 2
#### Login
![user2_login](./Images/user2_login.png)
#### Read
![user2_read](./Images/user2_read.png)
#### Write
![user2_write](./Images/user2_write.png)
# B
## 1. Variante
![db_before](./Images/db_before.png)
![snapshot](./Images/snapshot.png)
![collection_deleted](./Images/collection_deleted.png)
![snapshot_volume](./Images/snapshot_volume.png)
![db_after](./Images/db_after.png)
## 2. Variante
- `sudo mongodump --uri="mongodb://admin:1234@44.223.175.244:27017/?authSource=admin&readPreference=primary&ssl=false&directConnection=true" --db=kn02 --out "/home/ubuntu/dump"`
- `sudo mongorestore --uri="mongodb://admin:1234@44.223.175.244:27017/?authSource=admin&readPreference=primary&ssl=false&directConnection=true" "./dump"`
- `sudo mongosh -u admin -p 1234 --authenticationDatabase admin --host 44.223.175.244 --port 27017`
![dump_cmd](./Images/dump_cmd.png)
![db_dump2](./Images/db_dump2.png)
![drop_db](./Images/drop_db.png)
![monogrestore](./Images/monogrestore.png)
![restored_db](./Images/restored_db.png)
# C
### Replication vs. Partition (Sharding)
Replication and sharding are essential techniques in MongoDB to enhance database performance and reliability, but they serve different purposes. Replication creates multiple copies of the same data across different servers, ensuring high availability and fault tolerance. This is particularly beneficial for read-heavy applications as it allows for load balancing across multiple servers. Sharding, however, distributes data across multiple servers by dividing the dataset into smaller parts called shards. This approach is used to manage large datasets and improve both read and write performance by enabling horizontal scaling. While replication focuses on redundancy and high availability, sharding addresses the need for handling extensive data and high-throughput operations​ 
#### Replication
![](./Images/replication.png)
#### Partition (Sharding)
![](./Images/sharding.png)
### Recommendation to company
Given my company's Kahoot-like application, which uses MongoDB, I recommend implementing replication. Replication will ensure high availability and fault tolerance by maintaining multiple copies of the data on different servers. This setup will keep our application running smoothly even if one server fails and improve read performance by distributing read operations across multiple replicas. For a Kahoot-like application, having all data readily available is crucial. This type of application relies on real-time interactions and rapid data retrieval to ensure smooth gameplay and immediate feedback for users. Quizzes and games often require quick access to questions, answers, user scores, and other dynamic content.