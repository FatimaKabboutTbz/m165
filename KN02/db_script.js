db.createCollection("Streaming_Service", {
  validator: {
    $jsonSchema: {
      bsonType: "object",
      required: ["name", "launchDate"],
      properties: {
        name: {
          bsonType: "string",
        },
        launchDate: {
          bsonType: "date",
        },
        movieList: {
          bsonType: "array",
          items: {
            bsonType: "object",
            required: ["name", "releaseDate", "duration"],
            properties: {
              name: {
                bsonType: "string",
              },
              releaseDate: {
                bsonType: "date",
              },
              duration: {
                bsonType: "int",
              },
              ratings: {
                bsonType: "array",
                items: {
                  bsonType: "string",
                },
              },
              ageRating: {
                bsonType: "string",
              },
              actors: {
                bsonType: "array",
                items: {
                  bsonType: "objectId",
                },
              },
              genre: {
                bsonType: "objectId",
              },
            },
          },
        },
      },
    },
  },
});

db.createCollection("Genre", {
  validator: {
    $jsonSchema: {
      bsonType: "object",
      required: ["name", "ageRating"],
      properties: {
        name: {
          bsonType: "string",
        },
        ageRating: {
          bsonType: "string",
        },
        description: {
          bsonType: "string",
        },
      },
    },
  },
});


db.createCollection("Actor", {
  validator: {
    $jsonSchema: {
      bsonType: "object",
      required: ["name", "birthday"],
      properties: {
        name: {
          bsonType: "string",
        },
        birthday: {
          bsonType: "date",
        },
        height: {
          bsonType: "int",
        },
        gender: {
          bsonType: "string",
        },
      },
    },
  },
});
