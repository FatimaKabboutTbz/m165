![](/KN01/images/)

# A

![konzeptionelles_model](./images/konzeptionelles_model.png)

[Draw.io Datei zum konzeptionellen Model](./Movie_ERM.drawio)

Mein Konzeptionelles Model besteht aus vier Entitäten.

- Movie
- Genre
- Streamer
- Actor

Ich hatte die Idee eine DB zu erstellen, bei der ich Movies in Streaming-Services Verwalten kann.
Die Movie Entität hat eine One-to-Many Beziehung zu Streamer und Genre. Die Beziehung zwischen Movie und Actor ist Many-to-Many.

# B

![logisches_model](./images/logisches_model.png)

[Draw.io Datei zum logischem Model](./logisches_modell.drawio)

Die Verschachtelung von Filmen in Streaming-Services ordnet die Filmen zu ihren respektiven Streaming-Services zu und können so auch schnell und einfach filtriert werden.

# C

### commands

- `use kn02;`
- [Collections Skript](./db_script.js)
