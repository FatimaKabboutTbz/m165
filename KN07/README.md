# A
![login-instance](./Images/login-instance.png)

# B

![logisches_modell](./Images/logisches_modell.png)
- [draw.io](logisches_modell.drawio)

Die meisten Attribute übernahm ich von KN02, nur die Listen habe ich weggelassen, da sie hier nicht nötig sind. Bei den Kanten habe ich einen Attribut hinzugefügt, und zwar bei der Verbindung zwischen Movie und Actor. Das Attribut heisst ``roleType: String`` und drin können die verschiedenen Rollen eines Schauspielers gespeichert werden (Beispiel: leading, supporting, extra, etc.). Die restlichen Werte werden wie bei KN02 bei der MongoDB, hier bei den Knoten abgespeichert.