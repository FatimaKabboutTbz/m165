![]()

# A

- [Skript zum Daten hinzufügen](./insert.js)

# B

- [Skript das alle Collections löscht](./delete_collections.js)
- [Skript das teilweise Daten löscht](./delete_data.js)

# C

- [Skript mit den Abfragen](./read.js)

# D

- [Skript mit den Commands](./update.js)
