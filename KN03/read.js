db.Actor.find({ birthday: { $gt: new Date("1956-07-09") } });

db.Actor.find({$or: [{ height: { $lt: 183 } }, { height: { $gt: 150 } }]});

db.Genre.find({ $and: [{ ageRating: "PG-13" }, { name: "Action" } ]});

db.Genre.find({ name: { $regex: "a", $options: "i" } });

db.Streaming_Service.find({}, { name: "Netflix" });

db.Streaming_Service.find({}, { _id: 0, name: "Netflix" });