const actor1Id = new ObjectId();
const actor2Id = new ObjectId();
const actor3Id = new ObjectId();

const actor1Doc = {
  _id: actor1Id,
  name: "Tom Hanks",
  birthday: new Date("1956-07-09"),
  height: 183,
  gender: "male",
};

const actor2Doc = {
  _id: actor2Id,
  name: "Leonardo DiCaprio",
  birthday: new Date("1974-11-11"),
  height: 183,
  gender: "male",
};

const actor3Doc = {
  _id: actor3Id,
  name: "Jennifer Lawrence",
  birthday: new Date("1990-08-15"),
  height: 168,
  gender: "female",
};

db.Actor.insertOne(actor1Doc);
db.Actor.insertMany([actor2Doc, actor3Doc]);

const genre1Id = new ObjectId();
const genre2Id = new ObjectId();
const genre3Id = new ObjectId();

const genre1Doc = {
  _id: genre1Id,
  name: "Action",
  ageRating: "PG-13",
  description: "High-energy movies with lots of physical stunts and activities."
};

const genre2Doc = {
  _id: genre2Id,
  name: "Comedy",
  ageRating: "PG",
  description: "Movies that are designed to make the audience laugh."
};

const genre3Doc = {
  _id: genre3Id,
  name: "Drama",
  ageRating: "R",
  description: "Movies with serious narratives and characters in conflict."
};

db.Genre.insertOne(genre1Doc);
db.Genre.insertMany([genre2Doc, genre3Doc]);

const streamingServiceId1 = new ObjectId();
const streamingServiceId2 = new ObjectId();
const streamingServiceId3 = new ObjectId();


const streamingService1Doc = {
  _id: streamingServiceId1,
  name: "Netflix",
  launchDate: new Date("2007-01-16"),
  movieList: [
    {
      name: "The Matrix",
      releaseDate: new Date("1999-03-31"),
      duration: 136,
      ratings: ["5 stars", "4 stars", "5 stars"],
      ageRating: "R",
      actors: [actor1Id, actor2Id],
      genre: genre1Id
    },
    {
      name: "The Office",
      releaseDate: new Date("2005-03-24"),
      duration: 22,
      ratings: ["5 stars", "5 stars", "4 stars"],
      ageRating: "PG",
      actors: [actor2Id, actor3Id],
      genre: genre2Id
    }
  ]
};

const streamingService2Doc = {
  name: "Hulu",
  launchDate: new Date("2008-03-12"),
  movieList: [
    {
      name: "Inception",
      releaseDate: new Date("2010-07-16"),
      duration: 148,
      ratings: ["5 stars", "5 stars", "5 stars"],
      ageRating: "PG-13",
      actors: [actor1Id, actor3Id],
      genre: genre1Id
    },
    {
      name: "Friends",
      releaseDate: new Date("1994-09-22"),
      duration: 22,
      ratings: ["5 stars", "4 stars", "5 stars"],
      ageRating: "PG",
      actors: [actor2Id, actor3Id],
      genre: genre2Id
    }
  ]
};

const streamingService3Doc = {
  _id: streamingServiceId3,
  name: "Amazon Prime Video",
  launchDate: new Date("2006-09-07"),
  movieList: [
    {
      name: "The Boys",
      releaseDate: new Date("2019-07-26"),
      duration: 60,
      ratings: ["5 stars", "4 stars", "5 stars"],
      ageRating: "R",
      actors: [actor1Id, actor2Id],
      genre: genre2Id
    },
    {
      name: "The Marvelous Mrs. Maisel",
      releaseDate: new Date("2017-03-17"),
      duration: 57,
      ratings: ["5 stars", "5 stars", "5 stars"],
      ageRating: "PG-13",
      actors: [actor3Id, actor2Id],
      genre: genre3Id
    }
  ]
  
};

db.Streaming_Service.insertOne(streamingService1Doc);
db.Streaming_Service.insertMany([streamingService2Doc, streamingService3Doc]);

