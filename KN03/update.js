db.Actor.updateOne(
    { _id: ObjectId("66826597236e67f2a6246fd1") },
    { $set: { name: "Thomas Jeffrey Hanks" } }
  );
  
  db.Genre.updateMany(
    {
      $or: [
        { ageRating: "PG-13" },
        { name: "Action" }
      ]
    },
    {
      $set: { description: "Description was updated!" }
    }
  );
  

  db.Streaming_Service.replaceOne(
    { _id: ObjectId("66826598236e67f2a6246fd9") },
    {
      name: "Prime Video",
      launchDate: new Date("2016-12-14T00:00:00Z"),
      movieList: [
        {
          name: "The Boys",
          releaseDate: new Date("2019-07-26"),
          duration: 60,
          ratings: ["5 stars", "4 stars", "5 stars"],
          ageRating: "R",
          actors: [actor1Id, actor2Id],
          genre: genre2Id
        },
        {
          name: "The Marvelous Mrs. Maisel",
          releaseDate: new Date("2017-03-17"),
          duration: 57,
          ratings: ["5 stars", "5 stars", "5 stars"],
          ageRating: "PG-13",
          actors: [actor3Id, actor2Id],
          genre: genre3Id
        }
      ]    
    }
  );